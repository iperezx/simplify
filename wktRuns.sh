#!/bin/bash
g++ simplifyWKT.cpp -o simplifyWKT
for tuneParam in 0.25 0.5 0.75 1.00 1.25 1.50 5.00 10.00 20.00
do
    ./simplifyWKT maria/ignition.csv $tuneParam maria/original.txt \
    maria/simplified$tuneParam.txt maria/simplified$tuneParam.csv
done